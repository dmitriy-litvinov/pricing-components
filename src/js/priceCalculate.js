"use strict";

function CalculatePrice(types, data, choosenprod,  container) {
  this.types = types.type;
  this.data = data;
  this.container = container;
  this.container.innerHTML = '';
  this.selected = {};

  if ( data && Object.keys(data).length ) {
    this.markupForm();
  }

  if ( choosenprod && Object.keys(choosenprod).length ) {
    this.markupListGoods(choosenprod);
  }
}

CalculatePrice.prototype.events = function() {
  var typeSelect = document.querySelector('.type__list');
  var addToCalculate = document.querySelector('.add_to_calculate');
  var that = this;

  addToCalculate.addEventListener('click', this.chooseGoods.bind(this));

  typeSelect.addEventListener('change', function(e) {
    for ( var i = 0; i < this.children.length; i++ ) {
      if ( this.children[i].selected ) {
        that.selected['type'] = that.types[this.children[i].value];
        break;
      }
    }
    that.createModelSelect('.add_to_calculate');
  });
};

CalculatePrice.prototype.markupForm = function (isTitle) {
  var title = document.createElement('h2');
  this.form = document.createElement('form');
  var labelTypeSelect = document.createElement('label');
  var typeSelect = document.createElement('select');
  var option;
  var btn = document.createElement('button');
  var modelSelect;

  title.classList.add('main_title');
  title.innerText = 'Рассчет стоимости';

  this.form.classList.add('calculate_price');

  typeSelect.classList.add('type__list');
  typeSelect.setAttribute('id', 'choose_type');
  labelTypeSelect.setAttribute('for', 'choose_type');
  labelTypeSelect.innerHTML = '<span class="label_text">Тип товара</span>';

  this.types.forEach(function(item, i) {
    if (this.data[item.dataKey]) {
      if ( !this.selected['type'] ) {
        this.selected['type'] = item;
      }
      option = document.createElement('option');
      option.setAttribute('value', i);
      option.setAttribute('data-key', item.dataKey);
      option.innerText = item.webKey;
      typeSelect.appendChild(option);
    }
  }.bind(this));

  labelTypeSelect.appendChild(typeSelect);

  btn.setAttribute('type', 'button');
  btn.classList.add('add_to_calculate');
  btn.innerText = "Добавить";

  this.form.appendChild(labelTypeSelect);
  this.form.appendChild(btn);
  this.createModelSelect('.add_to_calculate');
  this.container.appendChild(title);
  this.container.appendChild(this.form);

  this.events();
};

CalculatePrice.prototype.createModelSelect = function (putBefore) {
  var select = this.form.querySelector('.model_select');
  if ( select ) {
    this.form.removeChild(select);
  }

  var data = this.data[this.selected.type.dataKey];
  var label = document.createElement('label');
  select = document.createElement('select');
  var option;

  select.classList.add('model__list');
  select.setAttribute('id', 'choose_model');
  label.setAttribute('for', 'choose_model');
  label.classList.add('model_select');
  label.innerHTML = '<span class="label_text">Модель</span>';

  data.forEach(function(item, i) {
    var key = Object.keys(item)[0];
    if ( i === 0 ) {
      this.selected['model'] = key;
      this.selected['price'] = item[key].price;
    }
    option = document.createElement('option');
    option.setAttribute('value', i);
    option.setAttribute('data-index', i);
    option.innerText = key;
    select.appendChild(option);
  }.bind(this));

  label.appendChild(select);
  this.form.insertBefore(label, this.form.querySelector(putBefore));

  select.removeEventListener('change', function () {});

  var that = this;
  select.addEventListener('change',function(e) {
    for ( var i = 0; i < this.children.length; i++ ) {
      if ( this.children[i].selected ) {
        var index = +this.children[i].getAttribute('data-index');
        var type = that.selected.type.dataKey;
        var prod = that.data[type][index];
        var model = Object.keys(that.data[type][index])[0];
        that.selected['model'] = model;
        that.selected['price'] = prod[model].price;
        break;
      }
    }
  }, false);
};

CalculatePrice.prototype.chooseGoods = function () {
  var type = this.selected.type.dataKey;
  var model = this.selected.model;
  var price = this.selected.price;
  var goods;
  var newgoods = {};
  var isCount = false;
  var products = this.data[type];

  if ( !localStorage.getItem('userChoose') ) {
    goods = {};
  } else {
    try {
      goods = JSON.parse(localStorage.getItem('userChoose'));
    } catch (m) {
      console.log(m);
    }
  }

  if ( goods[type] === undefined ) {
    newgoods[model] = {count: 1};
    newgoods[model]['price'] = price;
    newgoods[model]['webKey'] = this.selected.type.webKey;
    goods[type] = [newgoods];
    isCount = true;
  } else {
    goods[type].forEach(function (item, i) {
      if ( item[model] ) {
        item[model].count += 1;
        isCount = true;
        return false;
      }
    });

    if ( !isCount ) {
      newgoods[model] = {count: 1};
      newgoods[model]['price'] = price;
      newgoods[model]['webKey'] = this.selected.type.webKey;
      goods[type].push(newgoods);
    }
  }

  localStorage.setItem('userChoose', JSON.stringify(goods));
  this.markupListGoods(goods);
};

CalculatePrice.prototype.markupListGoods = function (goods) {
  var count = 1;
  var model;
  var type;
  var price;
  var num;
  var total = 0;
  var tableGoods = document.createElement('table');
  tableGoods.classList.add('goods');

  var tr = '<tr class="goods__item">'+
              '<th class="goods__num">№</th>'+
              '<th class="goods__name">Тип продукта</th>'+
              '<th class="goods__model">Модель</th>'+
              '<th class="goods__model">Кол-во</th>'+
              '<th class="goods__price">Цена, грн</th>'+
            '</tr>';

  for ( var key in goods ) {
    var elem = goods[key];

    for ( var i = 0; i < elem.length; i++ ) {
      model = Object.keys(elem[i])[0];
      type = elem[i][model].webKey;
      num = elem[i][model].count;
      price = elem[i][model].price * num;
      total += price;

      tr += '<tr class="goods__item">'+
               '<td class="goods__num">'+ count +'</td>'+
               '<td class="goods__name">'+ type +'</td>'+
               '<td class="goods__model">'+ model +'</td>'+
               '<td class="goods__model">'+ num +'</td>'+
               '<td class="goods__price">'+ price.toFixed(2) +'</td>'+
             '</tr>';
      count += 1;
    }
  }
    tr += '<tr class="goods__total">'+
             '<td></td>'+
             '<td></td>'+
             '<td></td>'+
             '<td class="total_title">Итого:</td>'+
             '<td class="total_price">'+ total.toFixed(2) +', грн</td>'+
           '</tr>';
  tableGoods.innerHTML = tr;

  var goods = document.querySelector('.goods');
  if ( goods ) {
    goods.parentNode.removeChild(goods);
  }

  document.querySelector('.conatainer_goods').appendChild(tableGoods);
};