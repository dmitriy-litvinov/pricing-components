"use strict";

if ( !localStorage.getItem('defaultType') ) {
  localStorage.setItem('defaultType', JSON.stringify({'type': [
                                                                {
                                                                  webKey: 'Материнская плата',
                                                                  dataKey: 'motherboard'
                                                                },
                                                                {
                                                                  webKey: 'Системный блок',
                                                                  dataKey: 'housing'
                                                                },
                                                                {
                                                                  webKey: 'Процессор',
                                                                  dataKey: 'processor'
                                                                },
                                                                {
                                                                  webKey: 'Опереативная память',
                                                                  dataKey: 'ram'
                                                                },
                                                              ]}));
}

var choosenprod;
var calculateContainer;
var createContainer;
var typesProduct;
var newProduct;
var dataGoods;
var calulateOrder;

window.onload = function() {
  createContainer = document.querySelector('.create_prod_container');
  calculateContainer = document.querySelector('.calculate_container');

  try {
    typesProduct = JSON.parse(localStorage.getItem('defaultType'));
    newProduct = new NewProduct(typesProduct.type, createContainer);
  } catch (m) {
    console.log(m);
  }

  if ( localStorage.getItem('goods') ) {
    dataGoods = JSON.parse(localStorage.getItem('goods'));
    try {
      if (localStorage.getItem('userChoose')) {
        choosenprod = JSON.parse(localStorage.getItem('userChoose'));
      }
      calulateOrder = new CalculatePrice(typesProduct, dataGoods, choosenprod, calculateContainer);
    } catch (m) {
      console.log(m);
    }
  }
};

