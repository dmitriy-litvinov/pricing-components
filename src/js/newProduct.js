"use strict";

function NewProduct(data, container) {
  this.data = data;
  this.container = container;
  this.values = {};
  this.markupForm();
  this.patterns = this.initPatterns();
}

NewProduct.prototype.events = function() {
  var select = document.querySelector('.create_prod__list');
  var textField = document.querySelectorAll('.text_field');
  var createProd = document.querySelector('.create_goods');
  var that = this;

  createProd.addEventListener('click', this.createGoods.bind(this));

  select.addEventListener('change', function(e) {
    that.validForm(this);
  });

  for ( var i = 0; i < textField.length; i++ ) {
    textField[i].addEventListener('change', function(e) {
      that.validForm(this);
    });
  }
};

NewProduct.prototype.markupForm = function() {
  var title = document.createElement('h2');
  var form = document.createElement('form');
  var labelSelect = document.createElement('label');
  var select = document.createElement('select');
  var option;
  var modelField = document.createElement('label');
  var priceField = document.createElement('label');
  var btn = document.createElement('button');

  title.classList.add('main_title');
  title.innerText = 'Создать комплектующее';

  form.classList.add('create_prod');

  select.classList.add('create_prod__list');
  select.setAttribute('id', 'create_type');
  select.setAttribute('required', 'required');
  labelSelect.setAttribute('for', 'create_type');
  labelSelect.innerHTML = '<span class="label_text">Тип</span>';

  modelField.setAttribute('for', 'model_field');
  modelField.innerHTML = '<span class="label_text">Модель</span><input type="text" pattern="model" required="required" id="model_field" class="text_field" placeholder="Модель"/><span class="error_text"></span>';

  priceField.setAttribute('for', 'price_field');
  priceField.innerHTML = '<span class="label_text">Цена</span><input type="text" pattern="price" required="required" id="price_field" class="text_field" placeholder="Цена"/><span class="error_text"></span>';

  btn.setAttribute('type', 'button');
  btn.setAttribute('disabled', 'disabled');
  btn.classList.add('create_goods');
  btn.innerText = "Создать";

  this.data.forEach(function(item, i) {
    if ( i === 0 ) {
      this.values['type'] = item.dataKey;
    }

    option = document.createElement('option');
    option.setAttribute('value', i);
    option.setAttribute('data-key', item.dataKey);
    option.innerText = item.webKey;
    select.appendChild(option);
  }.bind(this));

  labelSelect.appendChild(select);
  form.appendChild(labelSelect);
  form.appendChild(modelField);
  form.appendChild(priceField);
  form.appendChild(btn);

  this.container.innerHTML = '';
  this.container.appendChild(title);
  this.container.appendChild(form);

  this.events();
};

NewProduct.prototype.validForm = function(elem) {
  var isntEmptyFields = false;
  var form = elem.closest('form').elements;
  var isRequired = elem.getAttribute('required') ? true : false;
  var pattern = elem.getAttribute('pattern');
  var isVal = false;
  var btn = elem.closest('form').querySelector('button');
  var val;
  var index;

  if ( elem.nodeName == "SELECT" ) {
    for ( var i = 0; i < elem.children.length; i++ ) {
      if ( elem.children[i].selected ) {
        isVal = true;
        index = this.data[i].dataKey;
        break;
      }
    }
  } else {
    val = elem.value.trim();
    isVal = val.length ? true : false;
  }

  if ( isRequired && !isVal ) {
    elem.classList.add('empty');
    elem.nextElementSibling.innerText = this.patterns.errors[isRequiredMessage];
    btn.setAttribute('disabled', 'disabled');
    return false;
  } else if (elem.nodeName == "SELECT") {
    elem.classList.remove('empty');
    elem.nextElementSibling.innerText = '';
    this.values['type'] = index;
  } else if ( !this.patterns.pattern[pattern].test(val) ) {
    elem.classList.add('error');
    elem.nextElementSibling.innerText = this.patterns.errors[pattern];
    btn.setAttribute('disabled', 'disabled');
    return false;
  } else {
    elem.classList.remove('error');
    elem.nextElementSibling.innerText = '';
    this.values[pattern] = val;
  }

  for ( var j = 0; j < form.length; j++ ) {
    var field = form[j];
    if ( field.nodeName == "BUTTON" ) {
      continue;
    }

    if ( field.nodeName == "SELECT" ) {
      for ( var i = 0; i < field.children.length; i++ ) {
        if ( field.children[i].selected ) {
          isVal = true;
          break;
        }
      }
    } else {
      val = field.value.trim();
      isVal = val.length ? true : false;
    }

    if ( isRequired && !isVal ) {
      isntEmptyFields = false;
      return false;
    } else {
      isntEmptyFields = true;
    }
  }

  isntEmptyFields ? btn.removeAttribute('disabled') : btn.setAttribute('disabled', 'disabled');
};

NewProduct.prototype.initPatterns = function () {
  return  {
            pattern: {
              model: /[0-9,a-z,A-Z, \.-]{3,100}/g,
              price: /^[1-9]{1}[0-9]{0,4}\.[0-9]{2}$/g
            },
            errors : {
              isRequiredMessage: 'поле обязательно к заполнению',
              model: 'Данные не валидны, допустимые символы: 1-9 . a-z',
              price: 'Данные не валидны, формат цены 1.00'
            }
          }
};

NewProduct.prototype.createGoods = function () {
  var type = this.values.type;
  var model = this.values.model;
  var price = +this.values.price;
  var goods;
  var newgoods = {};

  if ( !localStorage.getItem('goods') ) {
    goods = {};
  } else {
    try {
      goods = JSON.parse(localStorage.getItem('goods'));
    } catch (m) {
      console.log(m);
    }
  }

  newgoods[model] = {price: price};

  goods[type] === undefined ? goods[type] = [newgoods] : goods[type].push(newgoods);
  localStorage.setItem('goods', JSON.stringify(goods));
  newProduct = new NewProduct(this.data, this.container);
  calulateOrder = new CalculatePrice(typesProduct, goods, choosenprod, calculateContainer);
};