'use strict';
import fs from 'fs';
import path from 'path';

import config  from'./build/gulp.config';
import initAliases from './build/aliases';

try {

    initTasks(config.paths.tasks, config);
    initAliases(config);

} catch (e) {
    console.error(e);
}

////////

function initTasks(tasksDir, config) {
    fs.readdirSync(tasksDir).forEach(taskFileName => {
        let taskFn = require(path.resolve(tasksDir, taskFileName));
        taskFn(config);
    });
}
