import gulp from 'gulp';
import notify from 'gulp-notify';
import connect from 'gulp-connect';
import concatJs  from 'gulp-concat';
import uglifyJs  from 'gulp-uglify';
import babel  from 'gulp-babel';

export default (config) => {
    gulp.task('scripts', function() {
        return gulp.src(config.paths.src.scripts.app)
            .pipe(concatJs(config.paths.dist.scripts.app.file))
            .pipe(babel({presets: ['es2015'], compact: false}))
            // .pipe(uglifyJs())
            .pipe(gulp.dest(config.paths.dist.scripts.app.dir))
            .pipe(notify('Done!'))
            .pipe(connect.reload());
    });

    gulp.task('libsJs', function() {
        return gulp.src(config.paths.src.scripts.libs)
          .pipe(concatJs(config.paths.dist.scripts.libs.file))
          .pipe(uglifyJs())
          .pipe(gulp.dest(config.paths.dist.scripts.libs.dir))
          .pipe(notify('Done!'))
          .pipe(connect.reload());
    });

    console.info('Gulp task *scripts* is loaded');
};