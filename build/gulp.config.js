export default {
    paths: {
        tasks: './build/tasks',
        src: {
            scripts:   {
                app: './src/js/**/*.js',
                libs: [
                    './node_modules/jquery/dist/jquery.min.js'
                ]
            },
            styles:    {
                app: './src/scss/style.scss',
                appAll: './src/scss/**/*.scss',
            },
            templates: [
                './src/*.html'
            ]
        },
        dist: {
            serverRoot: './dist',
            scripts: {
                app: {
                    file: 'app.js',
                    dir: './dist/js'
                },
                libs: {
                    file: 'libs.js',
                    dir: './dist/js'
                }
            },
            styles: {
                app: {
                    file: 'style.css',
                    dir: './dist/css'
                },
            },
            templates: {
                // file: 'index.html',
                dir: './dist'
            }
        }
    }
};