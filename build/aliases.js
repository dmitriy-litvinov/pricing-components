import gulp from 'gulp';

export default (config) => {

    // watch
    gulp.task('watch', function () {
        gulp.watch([ config.paths.src.styles.appAll ],  ['styles'] );
        gulp.watch([ config.paths.src.scripts.app ], ['scripts']  );
        gulp.watch([ config.paths.src.scripts.libs ], ['libsJs']  );
        gulp.watch([ config.paths.src.templates  ],  ['html']   );
    });
    
    // default  
    gulp.task('default', [
        'connect', 
        'html',
        'styles',
        'scripts',
        'libsJs',
        'watch'
    ]);
};