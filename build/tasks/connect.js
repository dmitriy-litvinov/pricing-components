import gulp    from 'gulp'; // require('gulp');
import connect from'gulp-connect';

export default (config) => { // module.exports = function() {...}

    gulp.task('connect', () => {
        connect.server({
            root: config.paths.dist.serverRoot,
            livereload: true,
            port: 3000
        });
    });

    console.info('Gulp task *connect* is loaded');
};
